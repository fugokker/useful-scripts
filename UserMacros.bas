Attribute VB_Name = "UserMacros"
' The file is encoded in ANSI CP-1251.
' Copyright 2023 by Muravev Kirill <fugokker@yandex.ru>. Licensed under CC BY-SA 4.0.

' Простановка пары типографских кавычек в тексте
Sub ТипографскиеКавычки()
Attribute ТипографскиеКавычки.VB_ProcData.VB_Invoke_Func = "Normal.NewMacros.ТипографскиеКавычки"
    Selection.TypeText Text:="«»"
    Selection.MoveLeft Unit:=wdCharacter, Count:=1
End Sub

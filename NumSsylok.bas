Attribute VB_Name = "NumSsylok"
' The file is encoded in ANSI CP-1251.
' Copyright 2023 by Muravev Kirill <fugokker@yandex.ru>. Licensed under CC BY-SA 4.0.

' Модификация полей, содержащих перекрёстные ссылки на таблицы и рисунки.
' Макрос исключает лидирующее название объекта ("Таблица", "Рисунок") из кода
' перекрёстной ссылки.

' Источник разработки: http://forum.oszone.net/nextnewesttothread-308009.html

' Муравьёв К. Ю. <fugokker@yandex.ru>, 2023
Sub NumSsylok()

    Dim CurField As Field
    Dim LinkText As String
    Dim OldCode As String
    Dim SCFlag, HTFlag As Boolean
    Dim RefsFound, RefsChanged As Long
    Const MsgBoxTitle = "Обработка ссылок на таблицы и рисунки"
    
    ' Предустановленные значения
    RefsFound = 0
    RefsChanged = 0
    HTFlag = False
    
    ' Выйти, если в документе нет полей
    If ActiveDocument.Fields.Count = 0 Then
        MsgBox "Документ не содержит полей.", vbExclamation, MsgBoxTitle
        Exit Sub
    End If
    
    ' Поиск перекрёстных ссылок
    For Each CurField In ActiveDocument.Fields
        If CurField.Type = wdFieldRef Then
            RefsFound = 1
            Exit For
        End If
    Next CurField
    
    ' Выйти, если в документе нет ссылок
    If RefsFound = 0 Then
        MsgBox "Документ не содержит перекрёстных ссылок.", vbExclamation, MsgBoxTitle
        Exit Sub
    End If
    
    RefsFound = 0
    
    ' Выбор опции восстановления скрытого текста в значениях полей
    Select Case MsgBox("Сделать видимым скрытый текст во всех полях ссылок?" & Chr(10) & Chr(13) & _
                       "Это действие необратимо.", vbYesNoCancel, MsgBoxTitle)
    Case vbYes
        HTFlag = True
    Case vbCancel
        Exit Sub
    End Select
    
    ' Перебор всех полей в открытом документе
    For Each CurField In ActiveDocument.Fields
        
        ' Выбрать только перекрёстные ссылки
        If CurField.Type = wdFieldRef Then
        
            'Запомнить состояние поля, показать его значение
            
            SCFlag = False
            
            If CurField.ShowCodes = True Then
                SCFlag = True
                CurField.Update
                CurField.ShowCodes = False
            End If
        
            ' Показ скрытого текста - необратимое изменение, влияющее на поля, которые не обязательно будут обработаны
            If HTFlag Then
                CurField.Select
                Selection.Font.Hidden = False
            End If
            
            LinkText = Trim(CurField.Result.Words(1).Text)
            
            ' Дополнить код ссылки, только если в нём ещё нет параметра "\#0".
            ' Если такой параметр уже есть в коде ссылки, то учесть его
            If InStr(CurField.code.Text, "# 0") = 0 And _
               InStr(CurField.code.Text, "#0") = 0 Then
            
                ' Выбрать только ссылки на таблицы и рисунки
                If LinkText = "Рисунок" Or _
                   LinkText = "Таблица" Or _
                   LinkText = "Figure" Or _
                   LinkText = "Table" Then
           
                    ' Показать код поля
                    CurField.ShowCodes = True
                    OldCode = CurField.code.Text
                
                    ' Вставка параметра "\#0" в код ссылки с помощью поиска внутри выделенного поля
                    CurField.Select
                    With Selection.Find
                        .ClearFormatting
                        .Replacement.ClearFormatting
                        .Text = "_Ref[0-9]@>"
                        .Replacement.Text = "^& ^0092#0"
                        .Format = False
                        .Forward = True
                        .MatchAllWordForms = False
                        .MatchCase = False
                        .MatchSoundsLike = False
                        .MatchWholeWord = False
                        .MatchWildcards = True
                        .Wrap = wdFindStop
                        .Execute Replace:=wdReplaceAll
                    End With
                    CurField.Update
                    
                    RefsFound = RefsFound + 1
                    If CurField.code.Text <> OldCode Then RefsChanged = RefsChanged + 1
                End If
            Else
                RefsFound = RefsFound + 1
            End If
            
            ' Восстановить состояние поля
            CurField.ShowCodes = SCFlag
        
        End If
    Next CurField
    
    ' Вывод статистики обработки
    If RefsFound > 0 Then
        If RefsFound = RefsChanged Then
            MsgBox "Найдено и обработано ссылок: " & RefsChanged, _
                   vbInformation, MsgBoxTitle
        Else
            MsgBox "Найдено ссылок: " & RefsFound & Chr(10) & Chr(13) & _
                   "Обработано ссылок: " & RefsChanged, _
                   vbInformation, MsgBoxTitle
        End If
    Else
        MsgBox "Ссылки для обработки не найдены.", vbExclamation, MsgBoxTitle
    End If
End Sub
